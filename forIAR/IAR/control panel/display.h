#include "iostm8s003f3.h"

#define segment_a 3
#define segment_b 7
#define segment_c 2
#define segment_d 5
#define segment_e 4
#define segment_f 4
#define segment_g 1
#define segment_dp 3

void bad_delay(void);

void init(void);

void light_number(unsigned short int);

void switch_on_digit(unsigned short int);

void switch_off_digit(unsigned short int);



