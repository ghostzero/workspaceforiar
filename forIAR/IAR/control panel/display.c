#include "display.h"

void bad_delay()
{
  long int i = 0;
  for (i = 0; i < 40000; i++);
}

void light_number(unsigned short int number)
{
  //turn off all giving to attention that ground of dig2 and dig3 on pc5 and pc6
  PC_ODR &= ~((1 << segment_a) | (1 << segment_b) | (1 << segment_f));
  PD_ODR &= ~((1 << segment_c) | (1 << segment_d) | (1 << segment_e) | (1 << segment_g));
  switch (number)
  {
    case 0:
      PC_ODR |= (1 << segment_a) | (1 << segment_b) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_e);
      break;
    case 1:
      PC_ODR |= (1 << segment_b);
      PD_ODR = (1 << segment_c);
      break;
    case 2:
      PC_ODR |= (1 << segment_a) | (1 << segment_b);
      PD_ODR = (1 << segment_d) | (1 << segment_e) | (1 << segment_g);
      break;
    case 3:
      PC_ODR |= (1 << segment_a) | (1 << segment_b);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_g);
      break;
    case 4:
      PC_ODR |= (1 << segment_b) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_g);
      break;
    case 5:
      PC_ODR |= (1 << segment_a) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_g);
      break;
    case 6:
      PC_ODR |= (1 << segment_a) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_e) | (1 << segment_g);
      break;
    case 7:
      PC_ODR |= (1 << segment_a) | (1 << segment_b);
      PD_ODR = (1 << segment_c);
      break;
    case 8:
      PC_ODR |= (1 << segment_a) | (1 << segment_b) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_e) | (1 << segment_g);
      break;
    case 9:
      PC_ODR |= (1 << segment_a) | (1 << segment_b) | (1 << segment_f);
      PD_ODR = (1 << segment_c) | (1 << segment_d) | (1 << segment_g);
      break;
  default:
    break;      
  }
}

void switch_on_digit(unsigned short int digit)
{
  switch (digit)
  {
    case 1:
      PA_ODR_bit.ODR3 = 0;
      PC_ODR_bit.ODR5 = 1;
      PC_ODR_bit.ODR6 = 1;
      PB_ODR_bit.ODR5 = 1;
      break;
    case 2:
      PA_ODR_bit.ODR3 = 1;
      PC_ODR_bit.ODR5 = 0;
      PC_ODR_bit.ODR6 = 1;
      PB_ODR_bit.ODR5 = 1;
      break;
    case 3:
      PA_ODR_bit.ODR3 = 1;
      PC_ODR_bit.ODR5 = 1;
      PC_ODR_bit.ODR6 = 0;
      PB_ODR_bit.ODR5 = 1;
      break;
    case 4:
      PA_ODR_bit.ODR3 = 1;
      PC_ODR_bit.ODR5 = 1;
      PC_ODR_bit.ODR6 = 1;
      PB_ODR_bit.ODR5 = 0;
      break;
  default:
      PA_ODR_bit.ODR3 = 0;
      PC_ODR_bit.ODR5 = 0;
      PC_ODR_bit.ODR6 = 0;
      PB_ODR_bit.ODR5 = 0;
    break;   
  }
}

void switch_off_digit(unsigned short int digit)
{
  switch (digit)
  {
     case 1:
      PA_ODR_bit.ODR3 = 1;
      break;
    case 2:
      PC_ODR_bit.ODR5 = 1;
      break;
    case 3:
      PC_ODR_bit.ODR6 = 1;
      break;
    case 4:
      PB_ODR_bit.ODR5 = 1;
      break;
  default:   
      PA_ODR_bit.ODR3 = 1;
      PC_ODR_bit.ODR5 = 1;
      PC_ODR_bit.ODR6 = 1;
      PB_ODR_bit.ODR5 = 1;
    break;   
  }
}

void init(void)
{ 
  PC_DDR_bit.DDR3 = 1; //a segment
  PC_DDR_bit.DDR7 = 1; //b segment
  PD_DDR_bit.DDR2 = 1; //c segment
  PD_DDR_bit.DDR5 = 1; //d segment
  PD_DDR_bit.DDR4 = 1; //e segment
  PC_DDR_bit.DDR4 = 1; //f segment
  PD_DDR_bit.DDR1 = 1; //g segment
  PD_DDR_bit.DDR3 = 1; //dot segment
  
  PB_DDR_bit.DDR5 = 1; //dig 4
  PC_DDR_bit.DDR6 = 1; //dig 3
  PC_DDR_bit.DDR5 = 1; //dig 2
  PA_DDR_bit.DDR3 = 1; //dig 1
  
  PC_CR1_bit.C13 = 1; //push-pull for a
  PC_CR2_bit.C23 = 0;  
  
  PC_CR1_bit.C17 = 1; //push-pull for b
  PC_CR2_bit.C27 = 0;
  
  PD_CR1_bit.C12 = 1; //push-pull for c
  PD_CR2_bit.C22 = 0;
  
  PD_CR1_bit.C15 = 1; //push-pull for d
  PD_CR2_bit.C25 = 0;
  
  PD_CR1_bit.C14 = 1; //push-pull for e
  PD_CR2_bit.C24 = 0;
  
  PC_CR1_bit.C14 = 1; //push-pull for f
  PC_CR2_bit.C24 = 0;
  
  PD_CR1_bit.C11 = 1; //push-pull for g
  PD_CR2_bit.C21 = 0;
  
  PD_CR1_bit.C13 = 1; //push-pull for dot
  PD_CR2_bit.C23 = 0;
  
  PB_CR1_bit.C15 = 1; //push-pull for dig 4
  PB_CR2_bit.C25 = 0;
  
  PC_CR1_bit.C16 = 1; //push-pull for dig 3
  PC_CR2_bit.C26 = 0;
  
  PC_CR1_bit.C15 = 1; //push-pull for dig 2
  PC_CR2_bit.C25 = 0;
  
  PC_CR1_bit.C13 = 1; //push-pull for dig 1
  PC_CR2_bit.C23 = 0;
  
  switch_off_digit(0);
  
}