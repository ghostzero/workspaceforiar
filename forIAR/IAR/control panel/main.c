#include "iostm8s003f3.h"
#include "display.h"


int main( void )
{
  init();
  int num = 0;
  int digit = 1;
    
  while(1)
  {
    light_number(num);
    switch_on_digit(digit);
    num++;
    digit++;
    if (num == 10) num = 0;
    if (digit == 5) digit = 1;
    delay();  
  }
  return 0;
}
