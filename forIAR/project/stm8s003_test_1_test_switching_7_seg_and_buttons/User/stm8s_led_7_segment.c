#include "iostm8s003f3.h"
//#define Led_A  PC_ODR_bit.ODR3
//#define Led_B  PC_ODR_bit.ODR7
//#define Led_C  PD_ODR_bit.ODR2
//#define Led_D  PD_ODR_bit.ODR5
//#define Led_E  PD_ODR_bit.ODR4
//#define Led_F  PC_ODR_bit.ODR4
//#define Led_G  PD_ODR_bit.ODR1
//#define Led_DP  PD_ODR_bit.ODR3
//#define Dig_1  PA_ODR_bit.ODR3
//#define Dig_2  PC_ODR_bit.ODR5
//#define Dig_3  PC_ODR_bit.ODR6
//#define Dig_4  PB_ODR_bit.ODR5




void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    if(current_number!=0){
      while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_4(i, b);
        current_number /= 10;
        i--;
      }
    }else{
      Set_segment_display_v_4(i, b);
    }
}

void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
          Dig_1=0;
          Dig_2=1;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          
          //            set_number(-1);
          break;
        }
        case 2:
        {
          
          Dig_1=1;
          Dig_2=0;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
            break;
        }
        case 3:
          {
            Dig_1=1;
            Dig_2=1;
            Dig_3=0;
            Dig_4=1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
           //            set_number(-1);
            break;
          }
        case 4:
        {
          Dig_1=1;
          Dig_2=1;
          Dig_3=1;
          Dig_4=0;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
          break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
  
  switch (Data_Number) {
  case 1:
    {
      view_one(); 
      
      break;
    }
  case 2:
    {
      view_two();
      
      break;
    }
  case 3:
    {
      view_three();
      
      break;
    }
  case 4:
    {
      view_four();
      break;
    }
  case 5:
    {
      view_five();
      break;
    }
  case 6:
    {
      view_six();
      
      break;
    }
  case 7:
    {
      view_seven();
      
      break;
    }
  case 8:
    {
      view_eight();
      
      break;
    }
  case 9:
    {
      view_nine();
      
      break;
    }
  case 0:
    {
      view_zero();
      
      break;
    }
  case -1:
    {
      view_minus_one();
      
      break;
    }
    
  }
  
}

void view_minus_one(void){
  Led_A = 0 ;
  Led_B =0;
  Led_C =0;
  Led_D=0;
  Led_E=0;
  Led_F=0;
  Led_G=0;
  //Led_DP=0;
}



void view_zero(void){
  Led_A = 1 ;
  Led_B =1;
  Led_C =1;
  Led_D=1;
  Led_E=1;
  Led_F=1;
  Led_G=0;
  //Led_DP=0;
}

void view_one(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_two(void){
Led_A = 1 ;
Led_B =1;
Led_C =0;
Led_D=1;
Led_E=1;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_three(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_four(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_five(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_six(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_seven(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_eight(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_nine(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}