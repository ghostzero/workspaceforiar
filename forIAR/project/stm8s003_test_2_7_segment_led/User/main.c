
#include "iostm8s003f3.h"
#define Led_A  PC_ODR_bit.ODR3
#define Led_B  PC_ODR_bit.ODR7
#define Led_C  PD_ODR_bit.ODR2
#define Led_D  PD_ODR_bit.ODR5
#define Led_E  PD_ODR_bit.ODR4
#define Led_F  PC_ODR_bit.ODR4
#define Led_G  PD_ODR_bit.ODR1
#define Led_DP  PD_ODR_bit.ODR3
#define Dig_1  PA_ODR_bit.ODR3
#define Dig_2  PC_ODR_bit.ODR5
#define Dig_3  PC_ODR_bit.ODR6
#define Dig_4  PB_ODR_bit.ODR5

void inicilazation_7_segment_led(void);
void view_one(void);
void view_two(void);
void view_three(void);
void view_four(void);
void view_five(void);
void view_six(void);
void view_seven(void);
void view_eight(void);
void view_nine(void);
void view_zero(void);
void view_minus_one(void);
void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void view_all_number_by_7_segments_by_4_elements(int current_number);

int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1;




void delay(unsigned int n)
{
    while (n-- > 0);
}

void main( void )
{
  int status_button_up = DEPRESSED_BUTTON;
  int count_button_up = 0;
  
  int status_button_menu = DEPRESSED_BUTTON;
  int count_button_menu = 0;
  
  int status_button_down = DEPRESSED_BUTTON;
  int count_button_down = 0;
  int output_to_led_dispaly = 21;
  
  inicilazation_7_segment_led();
  while (1)
  {
    
    
    view_all_number_by_7_segments_by_4_elements(output_to_led_dispaly);
    
    
//    Dig_1=1;
//  Dig_2=1;
//  Dig_3=1;
//  Dig_4=1;
    
//    Set_segment_display_v_4(4,1);
//    Set_segment_display_v_4(4,-1);
//    Set_segment_display_v_4(3,2);
//    Set_segment_display_v_4(2,3);
//    Set_segment_display_v_4(1,4);
    //    view_one();
//    view_two();
//    Dig_1=1;
//    Dig_2=1;
//    Dig_3=1;
//    Dig_4=0;
//    set_number(5);
    
  }
  
  
}

void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    if(current_number!=0){
      while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_4(i, b);
        current_number /= 10;
        i--;
      }
    }else{
      Set_segment_display_v_4(i, b);
    }
}


void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
          Dig_1=0;
          Dig_2=1;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          
          //            set_number(-1);
          break;
        }
        case 2:
        {
          
          Dig_1=1;
          Dig_2=0;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
            break;
        }
        case 3:
          {
            Dig_1=1;
            Dig_2=1;
            Dig_3=0;
            Dig_4=1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
           //            set_number(-1);
            break;
          }
        case 4:
        {
          Dig_1=1;
          Dig_2=1;
          Dig_3=1;
          Dig_4=0;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
          break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
  
  switch (Data_Number) {
  case 1:
    {
      view_one(); 
      
      break;
    }
  case 2:
    {
      view_two();
      
      break;
    }
  case 3:
    {
      view_three();
      
      break;
    }
  case 4:
    {
      view_four();
      break;
    }
  case 5:
    {
      view_five();
      break;
    }
  case 6:
    {
      view_six();
      
      break;
    }
  case 7:
    {
      view_seven();
      
      break;
    }
  case 8:
    {
      view_eight();
      
      break;
    }
  case 9:
    {
      view_nine();
      
      break;
    }
  case 0:
    {
      view_zero();
      
      break;
    }
  case -1:
    {
      view_minus_one();
      
      break;
    }
    
  }
  
}

void view_minus_one(void){
  Led_A = 0 ;
  Led_B =0;
  Led_C =0;
  Led_D=0;
  Led_E=0;
  Led_F=0;
  Led_G=0;
  //Led_DP=0;
}



void view_zero(void){
  Led_A = 1 ;
  Led_B =1;
  Led_C =1;
  Led_D=1;
  Led_E=1;
  Led_F=1;
  Led_G=0;
  //Led_DP=0;
}

void view_one(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_two(void){
Led_A = 1 ;
Led_B =1;
Led_C =0;
Led_D=1;
Led_E=1;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_three(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_four(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_five(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_six(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_seven(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_eight(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_nine(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}





void inicilazation_7_segment_led(void){
  
//  Led A ledpin_11 stmpin_13 port PC3
  PC_DDR_DDR3 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C13 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C23 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  
  //  Led B ledpin_7 stmpin_17 port PC7
  PC_DDR_DDR7 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C17 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C27 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led C ledpin_4 stmpin_19 port PD2
  PD_DDR_DDR2 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C12 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C22 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led D ledpin_2 stmpin_2 port PD5
  PD_DDR_DDR5 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C15 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C25 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led E ledpin_1 stmpin_1 port PD4
  PD_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led F ledpin_10 stmpin_14 port PC4
  PC_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led G ledpin_5 stmpin_18 port PD1
  PD_DDR_DDR1 = 1; //PortC, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C11 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C21 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led DP ledpin_3 stmpin_20 port PD3
  PD_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 1 ledpin_12 stmpin_10 port PA3
  PA_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PA_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PA_CR2_C23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 2 ledpin_9 stmpin_15 port PC5
  PC_DDR_DDR5 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PC_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 3 ledpin_8 stmpin_16 port PC6
  PC_DDR_DDR6 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C16 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C26 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 4 ledpin_6 stmpin_11 port PB5
  PB_DDR_DDR5 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PB_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PB_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  
  PA_ODR = 0; 
  PB_ODR = 0;
  PC_ODR = 0;
  PD_ODR = 0; //Turn off all pins
  Dig_1=1;
  Dig_2=1;
  Dig_3=1;
  Dig_4=1;
    
    
    
}
