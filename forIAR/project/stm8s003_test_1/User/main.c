/**
  ******************************************************************************
  * @file    Project/main.c 
  * @author  MCD Application Team
  * @version V2.2.0
  * @date    30-September-2014
  * @brief   Main program body
   ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 


/* Includes ------------------------------------------------------------------*/
//#include "stm8s.h"
#include "iostm8s003f3.h"


//#define 

void inicilazation_7_segment_led(void);


/* Private defines -----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void delay(unsigned int n)
{
    while (n-- > 0);
}

void main(void)
{
  
    inicilazation_7_segment_led();
    
    
    

    
    
  /* Infinite loop */
  while (1)
  {
    
    //  �������� ������ ������ �� ����������  
//    PA_ODR_bit.ODR1 = 0;
//    PA_ODR_bit.ODR3 = 0;
    //  �������� ��������� �
    PD_ODR_bit.ODR2 = 1;
    PD_ODR_bit.ODR1 = 0;
    
    
    
    
    //Turn on and off the output and then delay
//    PA_ODR_bit.ODR3=0;
//    PA_ODR_bit.ODR3 = !PA_ODR_bit.ODR3; 
//    PA_ODR_bit.ODR3 = 0;
//    PD_ODR_bit.ODR2 = 0;
//    PD_ODR_bit.ODR2 = !PD_ODR_bit.ODR2;
//    delay(30000);
    
    }
  
}


void inicilazation_7_segment_led(void){
  
//  Led A ledpin_11 stmpin_13 port PC3
  PA_DDR_DDR3 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PA_CR1_C13 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PA_CR2_C23 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  
  //  Led B ledpin_7 stmpin_17 port PC7
  PB_DDR_DDR7 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PB_CR1_C17 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PB_CR2_C27 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led C ledpin_4 stmpin_19 port PD2
  PD_DDR_DDR2 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C12 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C22 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led D ledpin_2 stmpin_2 port PD5
  PD_DDR_DDR5 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C15 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C25 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led E ledpin_1 stmpin_1 port PD4
  PD_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led F ledpin_10 stmpin_14 port PC4
  PC_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led G ledpin_5 stmpin_18 port PD1
  PD_DDR_DDR1 = 1; //PortC, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C11 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C21 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led DP ledpin_3 stmpin_20 port PD3
  PD_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_D13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_D23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 1 ledpin_12 stmpin_10 port PA3
  PD_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_D13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_D23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 2 ledpin_9 stmpin_10 port PA3
  PD_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_D13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_D23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 3 ledpin_8 stmpin_16 port PC6
  PC_DDR_DDR6 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_D16 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_D26 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  
  PA_ODR = 0; 
  PB_ODR = 0;
  PC_ODR = 0;
  PD_ODR = 0; //Turn off all pins
    
    
    
}











#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
