
#include "iostm8s003f3.h"

#define Led_A  PC_ODR_bit.ODR3
#define Led_B  PC_ODR_bit.ODR7
#define Led_C  PD_ODR_bit.ODR2
#define Led_D  PD_ODR_bit.ODR5
#define Led_E  PD_ODR_bit.ODR4
#define Led_F  PC_ODR_bit.ODR4
#define Led_G  PD_ODR_bit.ODR1
#define Led_DP  PD_ODR_bit.ODR3
#define Dig_1  PA_ODR_bit.ODR3
#define Dig_2  PC_ODR_bit.ODR5
#define Dig_3  PC_ODR_bit.ODR6
#define Dig_4  PB_ODR_bit.ODR5
#define MainSetViewDisplay 0
#define MainSetTemperature 1 
#define MainSetGysterezis  2 
#define MainSetTypeThermocouple 3 
#define MainSetCoefficient 4
#define MainSetValueCalibration 5

//#include "stm8s_led_7_segment.c"

void inicilazation_7_segment_led(void);
void inicilazation_7_segment_led_with_buttons_v_1(void);
void view_one(void);
void view_two(void);
void view_three(void);
void view_four(void);
void view_five(void);
void view_six(void);
void view_seven(void);
void view_eight(void);
void view_nine(void);
void view_zero(void);
void view_minus_one(void);
void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void view_all_number_by_7_segments_by_4_elements(int current_number);
int switching_to_inset(int current_switch_to_input, int total_count_switch_to_input,int current_count_button, int total_count_button );
int switching_to_inset_v_2(int current_switch_to_input, int total_count_switch_to_input);
void set_portA_by_number_with_CR(int number_port,int flag_input_output, int flag_zero_one_CR1, int flag_zero_one_CR2);
void set_portD_by_number_with_CR(int number_port,int flag_input_output, int flag_zero_one_CR1, int flag_zero_one_CR2);



int PRESSED_BUTTON = 0, DEPRESSED_BUTTON = 1; 


//void switching_to_button(unsigned char name_port,int number_port, int count_stroke,int current_count_button, int total_count_button){
//  
//}

// ��� ������������ � ��������� ��� 
int switching_to_inset(int current_switch_to_input, int total_count_switch_to_input,int current_count_button, int total_count_button ){
  if(current_switch_to_input <= total_count_switch_to_input){
    current_switch_to_input++;
  }else{
    current_switch_to_input = 0;
    return 1;
//    if(current_count_button <= total_count_button ){
//      current_count_button ++;
//    }else{
//      current_count_button = 0;
//      set_portD_by_number_with_CR(3,0,1,0);
//      return 1;
//    }
  }
  return 0;
}

int switching_to_inset_v_2(int current_switch_to_input, int total_count_switch_to_input){
  if(current_switch_to_input <= total_count_switch_to_input){
    current_switch_to_input++;
  }else{
    current_switch_to_input = 0;
    return 1;
  }
  return 0;
}




void set_portA_by_number_with_CR(int number_port,int flag_input_output, int flag_zero_one_CR1, int flag_zero_one_CR2){
  switch(number_port){
    case 0:{
      // port PA0
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 1:{
      // port PA1
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 2:{
      // port PA2
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 3:{
      // port PA3
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 4:{
      // port PA4
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 5:{
      // port PA5
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 6:{
      // port PA6
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 7:{
      // port PA7
      PA_DDR_DDR0 = flag_input_output; 
      PA_CR1_C10 = flag_zero_one_CR1; 
      PA_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
  }
}


void set_portD_by_number_with_CR(int number_port,int flag_input_output, int flag_zero_one_CR1, int flag_zero_one_CR2){
  switch(number_port){
    case 0:{
      // port PA0
      PD_DDR_DDR0 = flag_input_output; 
      PD_CR1_C10 = flag_zero_one_CR1; 
      PD_CR2_C20 = flag_zero_one_CR2; 
      break;
    }
    case 1:{
      // port PA1
      PD_DDR_DDR1 = flag_input_output; 
      PD_CR1_C11 = flag_zero_one_CR1; 
      PD_CR2_C21 = flag_zero_one_CR2; 
      break;
    }
    case 2:{
      // port PA2
      PD_DDR_DDR2 = flag_input_output; 
      PD_CR1_C12 = flag_zero_one_CR1; 
      PD_CR2_C22 = flag_zero_one_CR2; 
      break;
    }
    case 3:{
      // port PA3
      PD_DDR_DDR3 = flag_input_output; 
      PD_CR1_C13 = flag_zero_one_CR1; 
      PD_CR2_C23 = flag_zero_one_CR2; 
      break;
    }
    case 4:{
      // port PA4
      PD_DDR_DDR4 = flag_input_output; 
      PD_CR1_C14 = flag_zero_one_CR1; 
      PD_CR2_C24 = flag_zero_one_CR2; 
      break;
    }
    case 5:{
      // port PA5
      PD_DDR_DDR5 = flag_input_output; 
      PD_CR1_C15 = flag_zero_one_CR1; 
      PD_CR2_C25 = flag_zero_one_CR2; 
      break;
    }
    case 6:{
      // port PA6
      PD_DDR_DDR6 = flag_input_output; 
      PD_CR1_C16 = flag_zero_one_CR1; 
      PD_CR2_C26 = flag_zero_one_CR2; 
      break;
    }
    case 7:{
      // port PA7
      PD_DDR_DDR7 = flag_input_output; 
      PD_CR1_C17 = flag_zero_one_CR1; 
      PD_CR2_C27 = flag_zero_one_CR2; 
      break;
    }
  }
}






void delay(unsigned int n)
{
    while (n-- > 0);
}

void main( void )
{
  int status_button_up = DEPRESSED_BUTTON;
  int count_button_up = 0;
  
  int status_button_menu = DEPRESSED_BUTTON;
  int count_button_menu = 0;
  
  int status_button_down = DEPRESSED_BUTTON;
  int count_button_down = 0;
  int output_to_led_dispaly = 0;
//  inicilazation_7_segment_led_with_buttons_v_1();
  
  int current_switch_to_input = 0;
  int total_count_switch_to_input = 10;
  int current_count_button = 0;
  int total_count_button = 100;
  int flag_turn_on_button = 0; 
  int one_second = 0;
  int current_status_display=0;
  int set_temperature = 700;
  inicilazation_7_segment_led();
  unsigned long int current_time = 1;
  
  
  
  while (1)
  {
    
    if(current_time > 10000){
      current_time = 0;
      one_second = 0;
      current_status_display = MainSetViewDisplay;
    } else {
      current_time++;
    }
    
    //    if(PD_IDR_bit.IDR3==0){
    //      view_all_number_by_7_segments_by_4_elements(1);
    //    }else{
    //      view_all_number_by_7_segments_by_4_elements(2);
    //    }
    
    //    PD_DDR_DDR3 = 0; //PortD, Bit 2 is input (PD3 - Data Direction Register)
    //  PD_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
    //  PD_CR2_C23 = 0;
    
    
    //    
    if(one_second>10){
      one_second=0;
      current_status_display = MainSetTemperature; 
//      view_all_number_by_7_segments_by_4_elements(700);
    }else{
//      view_all_number_by_7_segments_by_4_elements(output_to_led_dispaly);
    }
//    MainSetViewDisplay=0, MainSetTemperature = 1, MainSetGysterezis= 2, MainSetTypeThermocouple=3,MainSetCoefficient = 4, MainSetValueCalibration = 5;
    switch(current_status_display){
//      MainSetViewDisplay
      case MainSetViewDisplay:{
        view_all_number_by_7_segments_by_4_elements(output_to_led_dispaly);
        break;
      }
      case MainSetTemperature:{
              view_all_number_by_7_segments_by_4_elements(set_temperature);
        break;
      }
    }
    
    
//    view_all_number_by_7_segments_by_4_elements(output_to_led_dispaly);
//    flag_turn_on_button = switching_to_inset_v_2(current_switch_to_input,total_count_switch_to_input);
    if(current_switch_to_input <= total_count_switch_to_input){
      current_switch_to_input++;
    }else{
      current_switch_to_input = 0;
      flag_turn_on_button = 1;
    }
    
    if(flag_turn_on_button == 1){
      flag_turn_on_button = 0;
     Dig_1=0;
      Dig_2=0;
      Dig_3=0;
      Dig_4=0;
    
      
      set_portD_by_number_with_CR(3,0,1,0);
      Dig_1=1;
      Dig_2=1;
      Dig_3=1;
      Dig_4=1;
    
     
//      for(int i=0;i<=100;i++){}
//      if (PD_IDR_bit.IDR3 == PRESSED_BUTTON) {
//        status_button_down = PRESSED_BUTTON;
//      }
//      
//      
//      if (status_button_down == PRESSED_BUTTON) {
//        count_button_down++;
//      }
      
      if (PD_IDR_bit.IDR3 == PRESSED_BUTTON) {
        status_button_down = PRESSED_BUTTON;
        count_button_down++;
      }
      
      if (status_button_down == PRESSED_BUTTON && count_button_down >= 50) {
        count_button_down = 0;
        status_button_down = DEPRESSED_BUTTON;
        
        switch(current_status_display){
          //      MainSetViewDisplay
        case MainSetViewDisplay:{
          output_to_led_dispaly++;
          one_second++;
          break;
        }
        case MainSetTemperature:{
          set_temperature++;
          current_time = 0;
          break;
        }
    }
//        one_second++;        

      } 
      set_portD_by_number_with_CR(3,1,1,1);
      
    }
    
    //    if(PD_IDR_bit.IDR3==0){
    //      count_button_down++;
    //    }
    
    
    
    //    view_all_number_by_7_segments_by_4_elements(output_to_led_dispaly);
  }
}




void view_all_number_by_7_segments_by_4_elements(int current_number) {
    //    int first_number = -1;
    //    int second_number = -1;
    //    int third_number = -1;
    //    int fourth_number = -1;
    int b = 0;
    int i = 4;
    if(current_number!=0){
      while (current_number > 0) {
        b = current_number % 10;
        b = current_number - (current_number / 10)*10;
        Set_segment_display_v_4(i, b);
        current_number /= 10;
        i--;
      }
    }else{
      Set_segment_display_v_4(i, b);
    }
}


void Set_segment_display_v_4(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
          Dig_1=0;
          Dig_2=1;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          
          //            set_number(-1);
          break;
        }
        case 2:
        {
          
          Dig_1=1;
          Dig_2=0;
          Dig_3=1;
          Dig_4=1;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
            break;
        }
        case 3:
          {
            Dig_1=1;
            Dig_2=1;
            Dig_3=0;
            Dig_4=1;
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
            set_number(Data_Byte);
           //            set_number(-1);
            break;
          }
        case 4:
        {
          Dig_1=1;
          Dig_2=1;
          Dig_3=1;
          Dig_4=0;
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          set_number(Data_Byte);
          //            set_number(-1);
          break;
        }
    }
    set_number(-1);
    //    Delay100TCYx(1);
}

void set_number(int Data_Number) {
  
  switch (Data_Number) {
  case 1:
    {
      view_one(); 
      
      break;
    }
  case 2:
    {
      view_two();
      
      break;
    }
  case 3:
    {
      view_three();
      
      break;
    }
  case 4:
    {
      view_four();
      break;
    }
  case 5:
    {
      view_five();
      break;
    }
  case 6:
    {
      view_six();
      
      break;
    }
  case 7:
    {
      view_seven();
      
      break;
    }
  case 8:
    {
      view_eight();
      
      break;
    }
  case 9:
    {
      view_nine();
      
      break;
    }
  case 0:
    {
      view_zero();
      
      break;
    }
  case -1:
    {
      view_minus_one();
      
      break;
    }
    
  }
  
}

void view_minus_one(void){
  Led_A = 0 ;
  Led_B =0;
  Led_C =0;
  Led_D=0;
  Led_E=0;
  Led_F=0;
  Led_G=0;
  //Led_DP=0;
}



void view_zero(void){
  Led_A = 1 ;
  Led_B =1;
  Led_C =1;
  Led_D=1;
  Led_E=1;
  Led_F=1;
  Led_G=0;
  //Led_DP=0;
}

void view_one(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_two(void){
Led_A = 1 ;
Led_B =1;
Led_C =0;
Led_D=1;
Led_E=1;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_three(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=0;
Led_G=1;
//Led_DP=0;
}

void view_four(void){
Led_A = 0 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_five(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_six(void){
Led_A = 1 ;
Led_B =0;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_seven(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=0;
Led_E=0;
Led_F=0;
Led_G=0;
//Led_DP=0;
}

void view_eight(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=1;
Led_F=1;
Led_G=1;
//Led_DP=0;
}

void view_nine(void){
Led_A = 1 ;
Led_B =1;
Led_C =1;
Led_D=1;
Led_E=0;
Led_F=1;
Led_G=1;
//Led_DP=0;
}





void inicilazation_7_segment_led(void){
  
//  Led A ledpin_11 stmpin_13 port PC3
  PC_DDR_DDR3 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C13 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C23 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  
  //  Led B ledpin_7 stmpin_17 port PC7
  PC_DDR_DDR7 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C17 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C27 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led C ledpin_4 stmpin_19 port PD2
  PD_DDR_DDR2 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C12 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C22 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led D ledpin_2 stmpin_2 port PD5
  PD_DDR_DDR5 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C15 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C25 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led E ledpin_1 stmpin_1 port PD4
  PD_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led F ledpin_10 stmpin_14 port PC4
  PC_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led G ledpin_5 stmpin_18 port PD1
  PD_DDR_DDR1 = 1; //PortC, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C11 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C21 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led DP ledpin_3 stmpin_20 port PD3
  PD_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 1 ledpin_12 stmpin_10 port PA3
  PA_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PA_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PA_CR2_C23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 2 ledpin_9 stmpin_15 port PC5
  PC_DDR_DDR5 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PC_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 3 ledpin_8 stmpin_16 port PC6
  PC_DDR_DDR6 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C16 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C26 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 4 ledpin_6 stmpin_11 port PB5
  PB_DDR_DDR5 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PB_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PB_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  
  PA_ODR = 0; 
  PB_ODR = 0;
  PC_ODR = 0;
  PD_ODR = 0; //Turn off all pins
  Dig_1=1;
  Dig_2=1;
  Dig_3=1;
  Dig_4=1;
    
    
    
}

void inicilazation_7_segment_led_with_buttons_v_1(void){
  
//  Led A ledpin_11 stmpin_13 port PC3
  PC_DDR_DDR3 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C13 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C23 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  
  //  Led B ledpin_7 stmpin_17 port PC7
  PC_DDR_DDR7 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C17 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C27 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led C ledpin_4 stmpin_19 port PD2
  PD_DDR_DDR2 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C12 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C22 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led D ledpin_2 stmpin_2 port PD5
  PD_DDR_DDR5 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C15 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C25 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led E ledpin_1 stmpin_1 port PD4
  PD_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PD_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led F ledpin_10 stmpin_14 port PC4
  PC_DDR_DDR4 = 1; //PortC, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C14 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C24 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led G ledpin_5 stmpin_18 port PD1
  PD_DDR_DDR1 = 1; //PortC, Bit 2 is output (PD3 - Data Direction Register)
  PD_CR1_C11 = 1; //PortA, Control Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C21 = 1; //PortA, Control Register 2, Bit 3 (PA3) set to Push-Pull
  //  Led DP ledpin_3 stmpin_20 port PD3
  PD_DDR_DDR3 = 0; //PortD, Bit 2 is input (PD3 - Data Direction Register)
  PD_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PD_CR2_C23 = 0; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 1 ledpin_12 stmpin_10 port PA3
  PA_DDR_DDR3 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PA_CR1_C13 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PA_CR2_C23 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 2 ledpin_9 stmpin_15 port PC5
  PC_DDR_DDR5 = 1; //PortD, Bit 2 is output (PD3 - Data Direction Register)
  PC_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 3 ledpin_8 stmpin_16 port PC6
  PC_DDR_DDR6 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PC_CR1_C16 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PC_CR2_C26 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  // Cathode digital character 4 ledpin_6 stmpin_11 port PB5
  PB_DDR_DDR5 = 1; //PortD, Bit 2 is output (PC3 - Data Direction Register)
  PB_CR1_C15 = 1; //PortA, Dontrol Register 1, Bit 3 (PA3) set to Push-Pull
  PB_CR2_C25 = 1; //PortA, Dontrol Register 2, Bit 3 (PA3) set to Push-Pull
  
  PA_ODR = 0; 
  PB_ODR = 0;
  PC_ODR = 0;
  PD_ODR = 0; //Turn off all pins
  Dig_1=1;
  Dig_2=1;
  Dig_3=1;
  Dig_4=1;
    
}

